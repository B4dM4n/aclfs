package aclfs

import (
	"bytes"
	"context"
	"os"
	"path"
	"regexp"
	"strings"
	"sync"
	"syscall"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"github.com/gobwas/glob"
	"github.com/pkg/errors"
	"gitlab.com/B4dM4n/acl"
)

const (
	aclXattrName        = "system.posix_acl_access"
	aclXattrName0       = aclXattrName + "\x00"
	aclXattrName00      = "\x00" + aclXattrName + "\x00"
	aclDefaultXattrName = "system.posix_acl_default"
)

type globRe struct{ r *regexp.Regexp }

func (g globRe) Match(s string) bool {
	return g.r.MatchString(s)
}

type ConfigEntry struct {
	Regexps []string
	Globs   []string
	ACLs    []string
}
type Config struct {
	Entries []ConfigEntry
}

type State struct {
	sync.RWMutex
	base    string
	filters []filter
	cache   map[string][]byte
}
type filter struct {
	matchers []glob.Glob
	acls     []acl.V2Entry
}
type node struct {
	*proxyNode
	state *State
}

func filtersFromConfig(conf *Config) ([]filter, error) {
	var filters []filter
	for _, c := range conf.Entries {
		var matchers []glob.Glob
		for _, s := range c.Regexps {
			re, err := regexp.Compile(s)
			if err != nil {
				return nil, errors.Wrap(err, "invalid Regexp")
			}
			matchers = append(matchers, globRe{re})
		}
		for _, s := range c.Globs {
			g, err := glob.Compile(s)
			if err != nil {
				return nil, errors.Wrap(err, "invalid Regexp")
			}
			matchers = append(matchers, g)
		}

		var acls []acl.V2Entry
		for _, s := range c.ACLs {
			a, err := acl.NewEntryV2FromString(s)
			if err != nil {
				return nil, errors.Wrap(err, "invalid acl")
			}
			acls = append(acls, a)
		}
		if len(acls) > 0 {
			filters = append(filters, filter{
				matchers: matchers,
				acls:     acls,
			})
		}
	}
	return filters, nil
}
func NewStateFromConfig(base string, conf *Config) (*State, error) {
	filters, err := filtersFromConfig(conf)
	if err != nil {
		return nil, err
	}
	state := &State{
		base:    base,
		cache:   make(map[string][]byte),
		filters: filters,
	}

	return state, nil
}

func (s *State) UpdateFilters(conf *Config) error {
	filters, err := filtersFromConfig(conf)
	if err != nil {
		return err
	}
	s.Lock()
	s.filters = filters
	s.cache = make(map[string][]byte)
	s.Unlock()
	return nil
}
func (s *State) get(p string) ([]byte, error) {
	suffix := strings.TrimPrefix(p, s.base)
	s.RLock()
	cache := s.cache
	resp, ok := cache[suffix]
	filters := s.filters
	s.RUnlock()
	if ok {
		return resp, nil
	}
	fi, err := os.Lstat(p)
	if err != nil {
		return nil, err
	}
	a := acl.FromMode(fi.Mode())
	b := path.Base(suffix)
	if suffix == "" {
		b = ""
	}
	for _, f := range filters {
		for _, m := range f.matchers {
			if m.Match(suffix) || m.Match(b) {
				a.Append(f.acls...)
				break
			}
		}
	}
	a.Fix()
	var buf bytes.Buffer
	_, err = a.Encode(&buf)
	if err != nil {
		return nil, err
	}
	resp = buf.Bytes()
	s.Lock()
	if len(filters) == len(s.filters) && (len(filters) == 0 || len(filters) > 0 && &filters[0] == &s.filters[0]) {
		s.cache[suffix] = resp
	}
	s.Unlock()
	return resp, nil
}

func (n *node) Getxattr(ctx context.Context, req *fuse.GetxattrRequest, resp *fuse.GetxattrResponse) error {
	switch req.Name {
	case aclXattrName:
		data, err := n.state.get(n.proxyNode.p)
		switch {
		case err != nil:
			return translateError(err)
		case len(data) == 0:
			return fuse.Errno(syscall.ENODATA)
		default:
			resp.Xattr = data
			return nil
		}
	case aclDefaultXattrName:
		return fuse.Errno(syscall.ENODATA)
	default:
		return n.proxyNode.Getxattr(ctx, req, resp)
	}
}
func (n *node) Listxattr(ctx context.Context, req *fuse.ListxattrRequest, resp *fuse.ListxattrResponse) error {
	err := n.proxyNode.Listxattr(ctx, req, resp)
	if !strings.HasPrefix(string(resp.Xattr), aclXattrName0) && !strings.Contains(string(resp.Xattr), aclXattrName00) {
		resp.Append(aclXattrName)
	}
	return err
}
func (n *node) Setxattr(ctx context.Context, req *fuse.SetxattrRequest) error {
	if req.Name == aclXattrName || req.Name == aclDefaultXattrName {
		return fuse.Errno(syscall.ENOTSUP)
	}
	return n.proxyNode.Setxattr(ctx, req)
}
func (n *node) Removexattr(ctx context.Context, req *fuse.RemovexattrRequest) error {
	if req.Name == aclXattrName || req.Name == aclDefaultXattrName {
		return fuse.Errno(syscall.ENOTSUP)
	}
	return n.proxyNode.Removexattr(ctx, req)
}

func (n *node) Lookup(ctx context.Context, name string) (nn fs.Node, err error) {
	nn, err = n.proxyNode.Lookup(ctx, name)
	if nn != nil {
		nn = &node{
			state:     n.state,
			proxyNode: nn.(*proxyNode),
		}
	}
	return
}

func (n *node) Symlink(ctx context.Context, req *fuse.SymlinkRequest) (nn fs.Node, err error) {
	nn, err = n.proxyNode.Symlink(ctx, req)
	if nn != nil {
		nn = &node{
			state:     n.state,
			proxyNode: nn.(*proxyNode),
		}
	}
	return
}

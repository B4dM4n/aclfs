package aclfs

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"path"
	"syscall"
	"time"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"github.com/pkg/errors"
	"github.com/pkg/xattr"
)

type proxyNode struct {
	p string
}

func newProxyNode(p string) *proxyNode {
	return &proxyNode{p}
}

func (d *proxyNode) Attr(ctx context.Context, attr *fuse.Attr) error {
	fi, err := os.Lstat(d.p)
	if err != nil {
		return translateError(err)
	}
	switch _fi := fi.Sys().(type) {
	case *syscall.Stat_t:
		attr.Inode = _fi.Ino
		attr.Size = uint64(_fi.Size)
		attr.Atime = time.Unix(_fi.Atim.Unix())
		attr.Mtime = time.Unix(_fi.Mtim.Unix())
		attr.Ctime = time.Unix(_fi.Ctim.Unix())
		//attr.Mode = os.FileMode(_fi.Mode)
		attr.Nlink = uint32(_fi.Nlink)
		attr.Uid = _fi.Uid
		attr.Gid = _fi.Gid
		attr.Rdev = uint32(_fi.Rdev)
	default:
		attr.Mtime = fi.ModTime()
	}
	attr.Mode = fi.Mode()
	return nil
}

func (d *proxyNode) Lookup(ctx context.Context, name string) (fs.Node, error) {

	return newProxyNode(path.Join(d.p, name)), nil
}

func (d *proxyNode) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	fd, err := os.Open(d.p)
	if err != nil {
		return nil, translateError(err)
	}
	h := proxyHandle{fd: fd}
	return h.ReadDirAll(ctx)
}

func (d *proxyNode) Open(ctx context.Context, req *fuse.OpenRequest, resp *fuse.OpenResponse) (fs.Handle, error) {
	fd, err := os.OpenFile(d.p, int(req.Flags), 0)
	if err != nil {
		return nil, translateError(err)
	}
	return &proxyHandle{fd: fd}, nil
}

func (d *proxyNode) Symlink(ctx context.Context, req *fuse.SymlinkRequest) (fs.Node, error) {
	newPath := path.Join(d.p, req.NewName)
	err := os.Symlink(newPath, req.Target)
	if err != nil {
		return nil, translateError(err)
	}
	return newProxyNode(newPath), nil
}
func (d *proxyNode) Readlink(ctx context.Context, req *fuse.ReadlinkRequest) (string, error) {
	return os.Readlink(d.p)
}

func (d *proxyNode) Getxattr(ctx context.Context, req *fuse.GetxattrRequest, resp *fuse.GetxattrResponse) error {
	data, err := xattr.LGet(d.p, req.Name)
	if err == nil {
		resp.Xattr = data
	}
	return translateError(err)
}
func (d *proxyNode) Listxattr(ctx context.Context, req *fuse.ListxattrRequest, resp *fuse.ListxattrResponse) error {
	names, err := xattr.LList(d.p)
	resp.Append(names...)
	return translateError(err)

}
func (d *proxyNode) Setxattr(ctx context.Context, req *fuse.SetxattrRequest) error {
	err := xattr.LSet(d.p, req.Name, req.Xattr)
	return translateError(err)
}
func (d *proxyNode) Removexattr(ctx context.Context, req *fuse.RemovexattrRequest) error {
	err := xattr.LRemove(d.p, req.Name)
	return translateError(err)
}

var (
	_ fs.Node               = (*proxyNode)(nil)
	_ fs.NodeStringLookuper = (*proxyNode)(nil)
	_ fs.HandleReadDirAller = (*proxyNode)(nil)
	_ fs.NodeReadlinker     = (*proxyNode)(nil)
	_ fs.NodeSymlinker      = (*proxyNode)(nil)
	_ fs.NodeGetxattrer     = (*proxyNode)(nil)
	_ fs.NodeListxattrer    = (*proxyNode)(nil)
	_ fs.NodeSetxattrer     = (*proxyNode)(nil)
	_ fs.NodeRemovexattrer  = (*proxyNode)(nil)
)

type proxyHandle struct {
	fd *os.File
}

func (h *proxyHandle) Flush(ctx context.Context, req *fuse.FlushRequest) error {
	return nil
}
func (h *proxyHandle) ReadAll(ctx context.Context) ([]byte, error) {
	return ioutil.ReadAll(h.fd)
}
func (h *proxyHandle) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	dirInfos, err := h.fd.Readdir(-1)
	if err != nil {
		return nil, translateError(err)
	}
	dirEnts := make([]fuse.Dirent, 0, len(dirInfos))
	for _, e := range dirInfos {
		dt := direntTypeFromMode(e.Mode())
		if dt == fuse.DT_Unknown {
			continue
		}
		dirEnts = append(dirEnts, fuse.Dirent{
			Inode: 0,
			Name:  e.Name(),
			Type:  dt,
		})
	}
	return dirEnts, nil
}
func (h *proxyHandle) Read(ctx context.Context, req *fuse.ReadRequest, resp *fuse.ReadResponse) error {
	data := make([]byte, req.Size)
	n, err := h.fd.ReadAt(data, req.Offset)
	if err == io.EOF {
		err = nil
	} else if err != nil {
		return translateError(err)
	}
	resp.Data = data[:n]
	return nil

}
func (h *proxyHandle) Write(ctx context.Context, req *fuse.WriteRequest, resp *fuse.WriteResponse) error {
	n, err := h.fd.WriteAt(req.Data, req.Offset)
	if err != nil {
		return translateError(err)
	}
	resp.Size = int(n)
	return nil

}
func (h *proxyHandle) Release(ctx context.Context, req *fuse.ReleaseRequest) error {
	return translateError(h.fd.Close())
}

func translateError(err error) error {
	if err == nil {
		return nil
	}
	cause := errors.Cause(err)
	switch cause {
	case nil:
		return err
	case os.ErrNotExist:
		return fuse.ENOENT
	case os.ErrExist:
		return fuse.EEXIST
	case os.ErrPermission:
		return fuse.EPERM
	case os.ErrClosed:
		return fuse.Errno(syscall.EBADF)
	case os.ErrInvalid:
		return fuse.Errno(syscall.EINVAL)
	}
	switch err := cause.(type) {
	case syscall.Errno:
		return fuse.Errno(err)
	case *xattr.Error:
		return translateError(err.Err)
	}

	return err
}

func direntTypeFromMode(m os.FileMode) fuse.DirentType {
	switch m & os.ModeType {
	case 0:
		return fuse.DT_File
	case os.ModeDir:
		return fuse.DT_Dir
	case os.ModeSymlink:
		return fuse.DT_Link
	case os.ModeNamedPipe:
	case os.ModeSocket:
	case os.ModeDevice:
	case os.ModeIrregular:
	}
	return fuse.DT_Unknown
}

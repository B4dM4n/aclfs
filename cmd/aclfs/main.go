package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/B4dM4n/aclfs"
)

func loadConfig(f string) (*aclfs.Config, error) {
	fd, err := os.Open(f)
	if err != nil {
		return nil, err
	}
	defer fd.Close()
	dec := json.NewDecoder(fd)
	var conf aclfs.Config
	err = dec.Decode(&conf)
	if err != nil {
		return nil, err
	}
	return &conf, nil
}

func main() {
	fConfig := flag.String("c", "config.json", "config file")
	flag.Parse()

	args := flag.Args()
	if len(args) != 2 || *fConfig == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	targetPath, mntPath := args[0], args[1]

	conf, err := loadConfig(*fConfig)
	if err != nil {
		log.Fatal(err)
	}
	state, err := aclfs.NewStateFromConfig(targetPath, conf)
	if err != nil {
		log.Fatal(err)
	}

	exit := make(chan struct{})
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)

		select {
		case <-c:
			log.Println(aclfs.Unmount(mntPath))
		case <-exit:
		}
	}()
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGHUP)

		for {
			select {
			case <-c:
				conf, err := loadConfig(*fConfig)
				if err != nil {
					log.Printf("Reloading config failed: %s", err)
					continue
				}
				err = state.UpdateFilters(conf)
				if err != nil {
					log.Printf("Reloading config failed: %s", err)
					continue
				}
				log.Printf("Reloading config done")
			case <-exit:
				return
			}
		}
	}()
	aclfs.Mount(targetPath, mntPath, state)
	close(exit)
}

module gitlab.com/B4dM4n/aclfs

go 1.14

require (
	bazil.org/fuse v0.0.0-20200424023519-3c101025617f
	github.com/gobwas/glob v0.2.3
	github.com/pkg/errors v0.9.1
	github.com/pkg/xattr v0.4.1
	gitlab.com/B4dM4n/acl v0.1.0
	golang.org/x/sys v0.0.0-20200430082407-1f5687305801 // indirect
)

replace bazil.org/fuse v0.0.0-20200424023519-3c101025617f => github.com/B4dM4n/fuse v0.0.0-20200424125657-bfc6b87ee6dd

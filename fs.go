package aclfs

import (
	"log"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
)

type Fs struct {
	root *node
}

func Mount(target string, mntPath string, state *State) error {
	fuse.Debug = func(v interface{}) {
		log.Printf("FUSE: %v", v)
	}
	c, err := fuse.Mount(mntPath,
		fuse.MaxReadahead(128*1024),
		fuse.Subtype("aclfs"),
		fuse.FSName("aclfs"),
		fuse.AllowOther(),
		fuse.PosixACL(),
		fuse.DefaultPermissions(),
	)
	if err != nil {
		return err
	}
	f := &Fs{
		root: &node{
			proxyNode: newProxyNode(target),
			state:     state,
		}}

	server := fs.New(c, nil)

	err = server.Serve(f)
	closeErr := c.Close()
	if err == nil {
		err = closeErr
	}
	return err
}

func Unmount(mntPath string) error {
	return fuse.Unmount(mntPath)
}

func (f *Fs) Root() (fs.Node, error) {
	return f.root, nil
}

var (
	_ fs.FS = (*Fs)(nil)
)
